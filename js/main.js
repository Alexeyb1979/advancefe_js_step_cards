class Visitor {
    constructor(doctorType, visitPuspose, patientName, comments){
        this.doctorType = doctorType;
        this.visitPuspose = visitPuspose;
        this.patientName = patientName;
        this.comments = comments;
    }

    static createModalWindow(){
        let modalBackground = document.createElement('div');
        modalBackground.className = 'modal-background';
        modalBackground.style.zIndex = 1000;
        modalBackground.onclick = (event) => {
                if(event.target === modalBackground){
                    modalBackground.remove();
                }
        }
        document.body.append(modalBackground);

        let modalForm = document.createElement('form');
        modalForm.className = 'modal-form';
        modalForm.submit = (event) => event.preventDefault();
        modalBackground.append(modalForm);

        let buttonClose = document.createElement('button');
        buttonClose.className = 'button-close';
        buttonClose.id = 'close-form';
        modalForm.append(buttonClose);
        buttonClose.innerHTML = 'x';
        buttonClose.onclick = () =>  modalBackground.remove(); 
        

        let modalBlockSelectDoctor = document.createElement('div');
        modalBlockSelectDoctor.className = 'modal-block';
        modalBlockSelectDoctor.id = 'modal-block-doctor';
        modalForm.append(modalBlockSelectDoctor);

        let doctorSelection = document.createElement('select');
        doctorSelection.className = 'doctor-selection';
        modalBlockSelectDoctor.append(doctorSelection);

        let selectOption = document.createElement('option');
        selectOption.textContent = 'Выберите доктора';
        doctorSelection.append(selectOption);

        ['Кардиолог','Стоматолог','Терапевт'].forEach(value => {
            let doctorOptions = document.createElement('option');
            doctorOptions.value = value;
            doctorOptions.textContent = value;
            doctorSelection.append(doctorOptions);
        });

        let modalBlockFields = document.createElement('div');
        modalBlockFields.className = 'modal-block';
        modalBlockFields.id = 'modal-block-fields';
        modalForm.append(modalBlockFields);

        let modalBlockComments = document.createElement('div');
        modalBlockComments.className = 'modal-block';
        modalBlockComments.id = 'modal-block-comments';
        modalForm.append(modalBlockComments);

        let submitButton = document.createElement('button');
        submitButton.className = "submit-button";
        submitButton.id = "submitButton";
        submitButton.innerHTML = 'submit';
        submitButton.onclick = (event) => {
            event.preventDefault()
            let selectedDoctor = new Visitor;
            selectedDoctor. createCard();
            modalBackground.remove();     
        };
        
        doctorSelection.onchange = () => {
            let clearInputFields = document.querySelector('#modal-block-fields');
                while (clearInputFields.firstChild) {
                    clearInputFields.removeChild(clearInputFields.firstChild);
                };

            let clearCommentsFields = document.querySelector('#modal-block-comments');
                while (clearCommentsFields.firstChild) {
                    clearCommentsFields.removeChild(clearCommentsFields.firstChild);
                };

            let selectedDoctor; 
                       
            if (doctorSelection.selectedOptions[0].value === 'Кардиолог'){
                selectedDoctor = new Cardiologist;
                
            } else if(doctorSelection.selectedOptions[0].value === 'Стоматолог'){
                selectedDoctor = new Dantist;
                
            } else if (doctorSelection.selectedOptions[0].value = 'Терапевт'){
                selectedDoctor = new Therapeuist;
            }

            selectedDoctor.addFormFields();
            modalForm.append(submitButton);
           
        }
    }

    addFormFields(){
      
       let modalBlockFields = document.querySelector('#modal-block-fields');
        let patientNameField = document.createElement('input');
        patientNameField.type = 'text';
        patientNameField.className = 'modal-input';
        patientNameField.id = 'patientName';
        patientNameField.placeholder = 'Ф. И. О.';
        patientNameField.required = true;
        modalBlockFields.append(patientNameField);

        let visitPusposeField = document.createElement('input');
        visitPusposeField.type = 'text';
        visitPusposeField.className = 'modal-input';
        visitPusposeField.id = 'visitField';
        visitPusposeField.placeholder = 'Цель визита';
        visitPusposeField.required = true;
        modalBlockFields.append(visitPusposeField);
        
    
    let modalBlockComments = document.querySelector('#modal-block-comments');   
    let commentaryField = document.createElement('input');
        commentaryField.type = 'text';
        commentaryField.className = 'modal-input';
        commentaryField.id = 'commentaryField';
        commentaryField.placeholder = 'Комментарии';
        commentaryField.required = false;
        modalBlockComments.append(commentaryField);
    
    };

    createCard(){
        let doctor =  document.querySelector('.doctor-selection');
        let selectedDoctor; 
                       
        if (doctor.selectedOptions[0].value === 'Кардиолог'){
            selectedDoctor = new Cardiologist;
            
        } else if(doctor.selectedOptions[0].value === 'Стоматолог'){
            selectedDoctor = new Dantist;
            
        } else if (doctor.selectedOptions[0].value = 'Терапевт'){
            selectedDoctor = new Therapeuist;
        }    

        Visitor.doctorType = doctor.selectedOptions[0].value;

        selectedDoctor.addCardFields() 
            // console.log(typeof(Visitor.doctorType));
    };
    
   addCardFields(){
        let cardForm = document.createElement('div');
        cardForm.className = 'card-form';
        let cardsField = document.querySelector("#field");
        cardsField.append(cardForm);

        let buttonCloseCard = document.createElement('button');
            buttonCloseCard.className = 'button-close';
            buttonCloseCard.id = 'close-card';
            cardForm.append(buttonCloseCard);
            buttonCloseCard.innerHTML = 'x';
            buttonCloseCard.onclick = () =>  cardForm.remove(); 

        let namePatientField = document.createElement('div');
        namePatientField.className = 'card-input';
        Visitor.patientName = document.querySelector("#patientName").value;
        namePatientField.innerHTML = Visitor.patientName;
        cardForm.append(namePatientField);

        let doctorTypeField = document.createElement('div');
        doctorTypeField.className = 'card-input';
        let doctor =  document.querySelector('.doctor-selection');
        Visitor.doctorType = doctor.selectedOptions[0].value;
        doctorTypeField.innerHTML = Visitor.doctorType;
        cardForm.append(doctorTypeField);

        let visitPurposeField = document.createElement('div');
            visitPurposeField.className = 'card-input';
            visitPurposeField.id = "visitPurposeField"
            Visitor.visitPuspose = document.querySelector('#visitField').value;
            visitPurposeField.innerHTML = Visitor.visitPuspose;
            visitPurposeField.hidden = true;
            cardForm.append(visitPurposeField);
        
        let commentaryCardField = document.createElement('div');
            commentaryCardField.className = 'card-input';
            commentaryField.id = 'commentary';
            Visitor.comments = document.querySelector('#commentaryField').value;
            commentaryCardField.innerHTML = Visitor.comments;
            commentaryField.hidden = true;
            cardForm.append(commentaryCardField);
        
        let buttonMoreInformation = document.createElement('button');
        buttonMoreInformation.id = 'moreInformation';
        cardForm.append(buttonMoreInformation);
        buttonMoreInformation.innerHTML = 'Показать Больше';
        buttonMoreInformation.onclick = (event) =>{
            event.preventDefault();
            buttonMoreInformation.remove();
            document.querySelector('#visitPurposeField').hidden = false;
            document.querySelector('#commentary').hidden = false;
        }
   }
};


    
class Cardiologist extends Visitor {
    constructor(doctorType, visitPuspose, patientName, comments, bloodPersure, bodyMassIndex, healthHeartProblems, patientAge){
        super(doctorType, visitPuspose, patientName, comments);
        this.bloodPersure = bloodPersure;
        this.bodyMassIndex = bodyMassIndex;
        this.healthHeartProblems = healthHeartProblems;
        this.patientAge = patientAge;
        }
    
    addFormFields(){
        
        super.addFormFields();
        // обычное давление, индекс массы тела, перенесенные заболевания сердечно-сосудистой системы, возраст, 
        const modalBlockFields = document.querySelector('#modal-block-fields');
        let bloodPersureField = document.createElement('input');
        bloodPersureField.type = 'text';
        bloodPersureField.className = 'modal-input';
        bloodPersureField.placeholder = 'Ваше обычное давление';
        bloodPersureField.required = true;
        modalBlockFields.append(bloodPersureField);

        let  bodyMassIndexField = document.createElement('input');
        bodyMassIndexField.type = 'text';
        bodyMassIndexField.className = 'modal-input';
        bodyMassIndexField.placeholder = 'Ваш индекс массы тела';
        bodyMassIndexField.required = true;
        modalBlockFields.append(bodyMassIndexField);

        let healthHeartProblemsField = document.createElement('input');
        healthHeartProblemsField.type = 'text';
        healthHeartProblemsField.className = 'modal-input';
        healthHeartProblemsField.placeholder = 'Перенесенные заболевания сердечно-сосудистой системы';
        healthHeartProblemsField.required = true;
        modalBlockFields.append(healthHeartProblemsField);

        let patientAgeField = document.createElement('input');
        patientAgeField.type = 'text';
        patientAgeField.className = 'modal-input';
        patientAgeField.placeholder = 'Возраст';
        patientAgeField.required = true;
        
        const visitPusposeField = document.querySelector('#visitField') ;
        visitPusposeField.before(patientAgeField);
    }
};

class Dantist extends Visitor {
    constructor(doctorType, visitPuspose, patientName, comments, lastVisitDate){
        super(doctorType, visitPuspose, patientName, comments);
        this.lastVisitDate = lastVisitDate;
        }

        addFormFields(){
            super.addFormFields();
            const modalBlockFields = document.querySelector('#modal-block-fields');
            let lastVisitField = document.createElement('input');
            lastVisitField.type = 'text';
            lastVisitField.className = 'modal-input';
            lastVisitField.placeholder = 'Дата последнего визита';
            lastVisitField.required = true;
            modalBlockFields.append(lastVisitField);
         }
};

class Therapeuist extends Visitor {
    constructor(doctorType, visitPuspose, patientName, comments, patientAge){
        super(doctorType, visitPuspose, patientName, comments);
        this.patientAge = patientAge;
        }
        addFormFields(){
        
            super.addFormFields();
            
            let patientAgeField = document.createElement('input');
            patientAgeField.type = 'text';
            patientAgeField.className = 'modal-input';
            patientAgeField.placeholder = 'Возраст';
            patientAgeField.required = true;
            
            const visitPusposeField = document.querySelector('#visitField') ;
    
            visitPusposeField.before(patientAgeField);
        }
    };

let createVisit = document.getElementById('createVisit');
createVisit.onclick = () => Visitor.createModalWindow();
